package ats;

import static org.junit.Assert.*;

import org.junit.Test;

public class AhorcadoTest {

	@Test
	public void palabraOcultaCasamientoDeberiaRetornarUnAstericoPorCaracter() {
		String palabraClave = "casamiento";
		
		Ahorcado ahorcado = new Ahorcado(palabraClave);
		
		assertEquals(ahorcado.getPalabraOculta(), "**********");
		
	}
	
	@Test
	public void palabraOcultaCasaDeberiaRetornarUnAstericoPorCaracter() {
		String palabraClave = "casa";
		
		Ahorcado ahorcado = new Ahorcado(palabraClave);
		
		assertEquals("****", ahorcado.getPalabraOculta());
		
	}
	
	
	@Test
	public void existeLetraCEnCasaDeberiaRetornarTrue() {
		String palabraClave = "casa";
		
		Ahorcado ahorcado = new Ahorcado(palabraClave);
		
		assertEquals(true, ahorcado.existeLetra('c'));
		
	}
	
	
	@Test
	public void existeLetraDEnCasaDeberiaRetornarFalse() {
		String palabraClave = "casa";
		
		Ahorcado ahorcado = new Ahorcado(palabraClave);
		
		assertEquals(false, ahorcado.existeLetra('d'));
	}
	
	
	
	@Test
	public void existeLetraDEnCasaConUnaVidaDeberiaRetornarFalseYPerderJuego() {
		String palabraClave = "casa";
		
		Ahorcado ahorcado = new Ahorcado(palabraClave);
		
		ahorcado.setVidas(1);
		
		ahorcado.existeLetra('d');
		
		assertEquals(0, ahorcado.getVidas());
		
	}
	
	
	@Test
	public void existeLetraCEnCasaTeniendoASAGanaElJuego() {
		String palabraClave = "casa";
		
		Ahorcado ahorcado = new Ahorcado(palabraClave);
		
		ahorcado.existeLetra('a');
		ahorcado.existeLetra('s');
		
		ahorcado.existeLetra('C');
		
		assertEquals(true, ahorcado.gano());
	}
	
	
	
	
	

}
