package ats;

import java.util.HashMap;

public class Ahorcado {

	private String palabraSecreta;
	private int vidas = 7;
	private int cantAciertos = 0;
	private HashMap<Character, Integer> hash;
	
	
	public int getVidas() {
		return vidas;
	}
	
	public void setVidas(int cantVidas) {
		vidas = cantVidas;
	}

	public Ahorcado(String palabraClave) {
		palabraSecreta = palabraClave;
		
		CharCounter cc = new CharCounter();
		
		hash = cc.countAll(palabraSecreta);  
	}

	public String getPalabraOculta() {
		
		String rta = "";
		
		for (int i=0; i<palabraSecreta.length();i++)
		{
			rta = rta + "*";
		}
		
		
		return rta;
		
	}

	public Object existeLetra(char c) {

		boolean rta = false;
		

		int cant = hash.get(String.valueOf(c).toLowerCase().toCharArray()[0]);
		
		if (cant>0)
		{
			cantAciertos = cantAciertos + cant;
			rta = true;
		}
		else
		{
			vidas = vidas - 1 ;
		}
		
		return rta;
	}
	
	public boolean gano() {
		
		boolean rta = false;
		
		if (palabraSecreta.length() == cantAciertos)
		{
			rta = true;
			
		}
		
		return rta;
	}


}
